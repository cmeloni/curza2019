<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Console;
use app\models\Validar;
use app\models\TblValidar;
use app\models\Query;
use app\models\FormUpload;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionHola($nav="Valor a recibir")
    {
        $variable = 'Valor a mostrar2';
        $mensaje = 'Mensaje...';
        $arreglo = ['Valor0', 'Valor1', 'Valor2', 'Valor3', 'Valor4', 'Valor5'];

        return $this->render('hola', 
            [
            'variable'=>$variable,
            'mostrar'=>$mensaje,
            'arreglo'=>$arreglo,
            'nav'=>$nav
            ]
        );
    }

    public function actionForm($mensaje=null)
    {
        return $this->render('form', ["mensaje"=>$mensaje]);
    }

    public function actionSform()
    {
        $mensaje=null;
         
        if (isset($_REQUEST["nombre"]))
        {
            $mensaje="El nombre es: ".$_REQUEST["nombre"];
        }
        return $this->redirect(['site/form', "mensaje"=>$mensaje]);
    }


    public function actionFormview($mensaje=null)
    {
        $model = new Query;

        if ($model->load(Yii::$app->request->get()))
        {
            if ($model->validate())
            {
                $search = Html::encode($model->query);
                $table = TblValidar::find()
                        ->where(['like', 'id', $search])
                        ->orWhere(['like', 'nombre', $search])
                        ->orWhere(['like', 'email', $search]);
            }
            else
            {
                $model->getErrors();
            }
        }
        else
        {
            $table = TblValidar::find();
        }
        $count = clone $table;
        $pages = new Pagination(
            [
                "pageSize"=>5,
                "totalCount"=> $count->count()
            ]);
        
        $data = $table->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();

        return $this->render('formview', 
                ["data"=>$data, 
                "model"=>$model , 
                'mensaje'=>$mensaje,
                'pages'=>$pages]);
    }

    public function actionDelusuario($id=null)
    {
        $mensaje = null;
        /*
        if (Yii::$app->user->isGuest)
        {
            $mensaje="No permitido...";
        }
        else
        {
        */
            $usr = TblValidar::findOne($id);
            $usr->delete();
        /*}*/
        return $this->redirect(['site/formview', "mensaje"=>$mensaje]);
    }

    public function actionValidar()
    {
        $model = new Validar;
        
        $mensaje = null;
         
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            //consultas, calculos, etc
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $tbl = new TblValidar;
                $tbl->nombre = $model->nombre;
                $tbl->email = $model->email;
                if ($tbl->insert())
                {
                    $mensaje="Su email fue guardado";
                    $model->nombre=null;
                    $model->email=null;
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
                //consultas, calculos, etc
            }
            else
            {
                $model->getErrors();
            }
        }
        return $this->render('validar', [
                'model'=>$model, 
                'mensaje'=>$mensaje
                ]);
    }

    public function actionActualizar($id = null)
    {
        $model = new Validar;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            //consultas, calculos, etc
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $usr = TblValidar::findOne($model->id);
                $usr->nombre=$model->nombre;
                $usr->email=$model->email;
                $usr->update();
                $mensaje = "Se guardaron correctamente los datos de: ".$usr->nombre;
            }
            else
            {
                $model->getErrors();
            }
        }
        else
        {
            $usr = TblValidar::findOne($id);

            $model->nombre = $usr->nombre;
            $model->email = $usr->email;
            $model->id=$id;

            $mensaje = "Modificando el Usuario: ".$usr->nombre;
        }        

        return $this->render('validar', [
            'model'=>$model, 
            'mensaje'=>$mensaje
            ]);
    }

    public function actionUpload()
    {
        $model = new FormUpload;
        $msg = null;
        
        if ($model->load(Yii::$app->request->post()))
        {
            $model->file = UploadedFile::getInstances($model, 'file');

            if ($model->file && $model->validate()) {
                foreach ($model->file as $file) {
                    $file->saveAs('archivos/' . $file->baseName . '.' . $file->extension);
                    $msg = "<p><strong class='label label-info'>Enhorabuena, subida realizada con éxito</strong></p>";
                }
            }
        }
        return $this->render("upload", ["model" => $model, "msg" => $msg]);
    }

    private function downloadFile($dir, $file, $extensions=[])
    {
        //Si el directorio existe
        if (is_dir($dir))
        {
            //Ruta absoluta del archivo
            $path = $dir.$file;
            
            //Si el archivo existe
            if (is_file($path))
            {
                //Obtener información del archivo
                $file_info = pathinfo($path);
                //Obtener la extensión del archivo
                $extension = $file_info["extension"];
                
                if (is_array($extensions))
                {
                //Si el argumento $extensions es un array
                //Comprobar las extensiones permitidas
                    foreach($extensions as $e)
                    {
                        //Si la extension es correcta
                        if ($e === $extension)
                        {
                            //Procedemos a descargar el archivo
                            // Definir headers
                            $size = filesize($path);
                            header("Content-Type: application/force-download");
                            header("Content-Disposition: attachment; filename=$file");
                            header("Content-Transfer-Encoding: binary");
                            header("Content-Length: " . $size);
                            // Descargar archivo
                            readfile($path);
                            //Correcto
                            return true;
                        }
                    }
                }
                
            }
        }
        //Ha ocurrido un error al descargar el archivo
        return false;
    }

    public function actionDownload()
    {
        if (Yii::$app->request->get("file"))
        {
            //Si el archivo no se ha podido descargar
            //downloadFile($dir, $file, $extensions=[])
            if (!$this->downloadFile("archivos/", Html::encode($_GET["file"]), ["pdf", "txt", "doc"]))
            {
                //Mensaje flash para mostrar el error
                Yii::$app->session->setFlash("errordownload");
            }
        }
        return $this->render("download");
    }

}

<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h1>Form de Prueba</h1>
<h3><?= $mensaje ?></h3>

<h3><?= $_ENV['DB_DATABASE'] ?></h3>

<?= Html::beginForm(
        Url::toRoute("site/sform"), //accion
        "get", //method
        ['class'=>'form-inline'] //options
);
?>

<div class="form-group">
    <?= Html::label("Nombre: ", "nombre") ?>
    <?= Html::textInput("nombre", null, ["class"=>"form-control"]) ?>
</div>

<?= Html::submitInput("Enviar el nombre", ["class"=>"btn btn-primary"]) ?>

<?= Html::endForm() ?>




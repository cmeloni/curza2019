<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = 'Validar';

?>

<h1> Formulario Validado </h1>

<h3><?= $mensaje ?></h3>

<a href="<?= Url::toRoute("site/formview") ?>">Lista</a>

<?php
    $form = ActiveForm::begin([
        "method"=>"post",
        "id" => "formulario",
        "enableClientValidation"=>false,
        "enableAjaxValidation"=>true
    ]);
?>

<div class="form-group">
    <?= $form->field($model, "id")->hiddenInput(['value'=> $model->id])->label(false) ?>
</div>

<div class="form-group">
    <?= $form->field($model, "nombre")->input("text") ?>
</div>

<div class="form-group">
    <?= $form->field($model, "email")->input("email") ?>
</div>

<?= Html::submitInput("Guardar", ["class"=>"btn btn-primary"]) ?>

<?php
    $form->end()
?>


<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "new_user".
 *
 * @property string $username
 * @property string $name
 * @property string $password
 * @property string $authkey
 * @property string $accesstoken
 */
class NewUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'new_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'name', 'password', 'authkey', 'accesstoken'], 'required'],
            [['username', 'name'], 'string', 'max' => 80],
            [['password', 'authkey', 'accesstoken'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'name' => 'Name',
            'password' => 'Password',
            'authkey' => 'Authkey',
            'accesstoken' => 'Accesstoken',
        ];
    }
}
